## Senior Data Scientist - Submission by Carl Stanley

I've included my results for all of the tasks in this folder, and you should be
able to get the code up and running with the requirements.txt file.

### Task 1: Data Cleaning
For this task I used pandas dataframes as that's what I'm most experienced with
. I read in the url (either specified by the user or just the one from the 
task if none given), and I clean it. I also accept another argument for 
"extra_split_args" which accepts a list of some of the less common delimiters 
that people working in the business for longer than me might be familiar with.

With more time (as I said in the code comments) I'd turn this into a class so
that we could save the metadata on the splitting arguments, to ensure 
continuity across the user base.

I also timestamp the output as I know that's a huge pain if not done correctly.

The code utilites both an argument parser for the above, as well as a logger 
for info. These can be found in utils.py.

#### Running task 1 on Unix
~~~
python -m venv tmp_venv/
source tmp_venv/bin/activate
pip install --upgrade pip
pip install -r requirements_1.txt

# Both --args are optional
python build_SDN_list_dataset.py --path --extra_split_args
~~~

#### Running task 1 on Windows
~~~
python -m venv tmp_venv/
source tmp_venv/Scripts/activate
pip install --upgrade pip
pip install -r requirements_1.txt

# Both --args are optional
python build_SDN_list_dataset.py --path --extra_split_args
~~~

### Task 2: Text Modelling
For this task I used BERT as suggested but I used a pre-trained model for times
sake. I ran the initial database through the class in the initialisation and 
then passed a string into the score function. 
The result of the score_list function is a dataframe that forms a similarity
matrix. This will show the cosine similarity between all of the data in the SDN
list, cross-tabbed with the inputs. 

#### Running task 2 on Unix (has the same differences for Windows as task 1)
~~~
python -m venv tmp_venv/
source tmp_venv/bin/activate
pip install --upgrade pip
pip install -r requirements_2.txt

# In the jupyter notebook
# Get access to the pre-trained BERT model
!wget https://raw.githubusercontent.com/JohnSnowLabs/nlu/master/scripts/colab_setup.sh -O - | bash

# Initialise the class (no args means it takes the ./output/split_names.csv)
cl = Comparison()
scores = score_list(path_to_input_data)
cl.score_chart
~~~


### Task 3: Interpretation
Regarding the top scoring names & companies, some of the companies appeared in
both the input and the database, giving them 1.0 cosine similarity. Obviously
we should take this into account, but there are also some names that are very
similar to quite a few of the database (e.g. Maria Rodruigez). This can be seen in the 
output/reduced_threat_plot.png file. We could utilise a multiplicative 
factor for the number of times that the similarity exceeds some threshold 
(I've set 0.93 as mine).

The utilisation of the mode is where business-oriented minds need to 
help with the decision. It's myrole as a data scientist to help them to 
understand what constitutes "high" 
when it comes to cosine similarity, but ultimately in order to make business
decisions off of the back of this, we need to understand what level of "high" 
actually poses a threat. 

I would recommend having a meeting where I run through some examples of 
the most similar names / companies, and then take feedback from the business. 
Then I would engineer a visualisation where all of these scores that exceed the
threshold are highlighted. Again, a business decision needs to be made 
regarding what to do with these problem candidates. What I don't want to do is
take a completely data driven approach, and end up answering either a different
version of the business' questions, or another question altogether. 

Regarding the reliability of the model, cosine similarity is accurate when the
embeddings are sensible, but as always there's a level of abstraction applied 
when we overlay the business context of our problem. In order to determine the
accuracy of the model, I'd like to do some retroactive testing where I see how
the model would've done, and compared it to the real outcomes.
