import argparse
import logging


def initialise_parser():
    """Function to initialise arg parsing."""
    parser = argparse.ArgumentParser()
    parser.add_argument('--path',
                        dest="path",
                        action="store",
                        default=None,
                        help="Path to data for reading in")

    parser.add_argument('--extra_split_args',
                        dest='extra_split_filters',
                        action="store",
                        default=[],
                        help="Extra argument for splitting names on\n \
                              Accepts a list of arguments.")
    return parser


def initialise_logger():
    """Function to initialise logger."""
    _logger = logging.getLogger('name_strip')
    _logger.setLevel(logging.INFO)

    sh = logging.StreamHandler()
    sh.setLevel(logging.INFO)

    formatter = logging.Formatter(
        '%(asctime)s - %(name)s - %(levelname)s - %(message)s')

    sh.setFormatter(formatter)
    _logger.addHandler(sh)
    return _logger
