import urllib.request
import pandas as pd

from utils import initialise_logger, initialise_parser

_logger = initialise_logger()


def per_section(iter_, is_delimiter=lambda x: x.isspace()):
    """Decode the text file line by line."""
    ret = []
    for line in iter_:
        line = line.decode("utf-8")
        if is_delimiter(line):
            if ret:
                yield ret  # OR  ''.join(ret)
                ret = []
        else:
            ret.append(line.rstrip())  # OR  ret.append(line)
    if ret:
        yield ret


def search_name(line, extra_args=[]):
    """Search the line for the specific separators of names."""
    args = ["(", ",", ";"]
    args.extend(extra_args)

    for a in args:
        if a in line:
            line = line.split(a)[0]
    return line


def remove_header_and_footer(data, start_lines=2, end_lines=7):
    """Remove unneccessary lines, but more configurable."""
    return data[start_lines:-end_lines]


def main():
    """Split the SDN list to identify the names.

    Arguments:
        Path: This is the path of the SDN list. If no path is given, I'll use
        the one provided in the task.
        extra_split_filters: This is an extra argument in case there's any
        ad-hoc analysis or field experts who know how to clean this data.

    Returns:
        stripped_names: A text file with the date of the run.

    With more time I would create a class to save the extra arguments over
    time.
    """

    parser = initialise_parser()
    args = parser.parse_args()

    if args.path is None:
        path = "https://www.treasury.gov/ofac/downloads/sdnlist.txt"
        _logger.info(f"No path specified - using default path:\n{path}")
    else:
        _logger.info(f"Loading in file from path:\n{args.path}")
        path = args.path

    # Skip top 20 lines as it's a description
    # No header as there is no header
    # Remove errors on bad lines as the data is not good at all
    data = urllib.request.urlopen(path)
    _logger.info("Loaded in - splitting names")
    data_list = list(per_section(data))

    for i in range(0, len(data_list)):
        data_list[i] = ' '.join(data_list[i])

    data_list_pruned = remove_header_and_footer(data_list)

    df = pd.DataFrame(data_list_pruned, columns=["original"])
    if len(args.extra_split_filters) > 0:
        _logger.info("You passed extra args: {}".format(
            args.extra_split_filters))
    df["strip_aka"] = df["original"].apply(
        lambda x: search_name(x, extra_args=args.extra_split_filters))
    _logger.info("Splitting complete - outputting file")

    today = pd.to_datetime("today").strftime('%Y-%m-%d')
    file = f"output/stripped_names_{today}.txt"

    df[["strip_aka"]].drop_duplicates(keep="first").to_csv(file,
                                                           sep='\n',
                                                           index=False,
                                                           header=False)
    _logger.info("Output created")


if __name__ == "__main__":
    main()
